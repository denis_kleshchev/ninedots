package com.example.mint.ninedots;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.CountDownTimer;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

public class NineDotsLayout extends RelativeLayout implements View.OnTouchListener{
    final String truePassword = "123456789";
    String tryPassword = "";
    private Context context;
    Paint p;
    ArrayList<Rect> lines= new ArrayList<Rect>();
    boolean drawing=false;
    int x1, y1;
    int x2, y2;
    Path path;
    boolean checkingBlocked = false;

    public NineDotsLayout(Context context, AttributeSet set) {
        super(context, set);
        this.context = context;
        this.setOnTouchListener(this);

        p = new Paint();
        p.setColor(Color.WHITE);
        p.setStyle(Paint.Style.STROKE);
        p.setStrokeWidth(5);
        p.setStrokeCap(Paint.Cap.ROUND);

        path = new Path();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        // TODO Auto-generated method stub
        super.onDraw(canvas);

        if (drawing)
            canvas.drawLine(x1, y1, x2, y2, p);

        canvas.drawPath(path, p);

    }

    @Override
    public boolean onTouch(View v, final MotionEvent event) {
        if (checkingBlocked) return false;

        final RelativeLayout relativeLayout = (RelativeLayout) v;
        boolean result = false;
        Point firstPoint = null;

        for (int i=0; i<relativeLayout.getChildCount(); i++){
            NineDotsView view = (NineDotsView)relativeLayout.getChildAt(i);
            Rect rect = new Rect(view.getLeft(), view.getTop(), view.getRight(), view.getBottom());
            if (rect.contains((int)event.getX(), (int)event.getY())) {
                if (!view.isHasChecked()) {
                    view.setHasChecked(true);
                    Log.d("111", "Drag over " + view.getMyNumber());
                    tryPassword += view.getMyNumber();

                    int middleWidth = (int)(view.getX() + view.getWidth()/2);
                    int middleHeight = (int)(view.getY() + view.getHeight()/2);

                    if (path.isEmpty()) {
                        path.moveTo(middleWidth, middleHeight);
                        firstPoint = new Point(middleWidth, middleHeight);
                    } else {
                        path.lineTo(middleWidth, middleHeight);

                        x1 = middleWidth;
                        y1 = middleHeight;
                    }
                }
            }
        }

        switch (event.getAction()){

            case MotionEvent.ACTION_DOWN:

                p.setColor(getResources().getColor(android.R.color.white));

                if (firstPoint == null) {
                    drawing = false;
                    result = false;
                    break;
                }

                x1=x2=firstPoint.x;
                y1=y2=firstPoint.y;
                drawing=true;
                result=true;
                break;

            case MotionEvent.ACTION_UP:
                checkingBlocked = true;
                Log.d ("111", "True pass: " + truePassword + ", you entered: " + tryPassword);
                if (truePassword.equals(tryPassword)) {
                    Toast.makeText(context, "True", Toast.LENGTH_SHORT).show();
                    p.setColor(getResources().getColor(android.R.color.holo_green_light));
                } else {
                    Toast.makeText(context, "Fail", Toast.LENGTH_SHORT).show();
                    p.setColor(getResources().getColor(android.R.color.holo_red_dark));
                }

                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        path.reset();
                        Log.d ("111", "2 secs gone");

                        for (int i=0; i<relativeLayout.getChildCount(); i++){
                            NineDotsView view = (NineDotsView)relativeLayout.getChildAt(i);
                            view.setHasChecked(false);
                        }

                        tryPassword = "";

                        invalidate();
                        checkingBlocked = false;
                    }
                }, 2000);
                x2=x1;
                y2=y1;
                drawing=false;
                result=true;
                break;

            case MotionEvent.ACTION_MOVE:
                x2 = (int)event.getX();
                y2= (int)event.getY();
                result=true;
                break;
        }
        if (result) invalidate();
        return result;
    }
}


