package com.example.mint.ninedots;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;

public class NineDotsView extends View {

    private boolean hasChecked;
    private String myNumber;

    public boolean isHasChecked() {
        return hasChecked;
    }

    public String getMyNumber() {
        return myNumber;
    }

    public void setHasChecked(boolean hasChecked) {
        this.hasChecked = hasChecked;
    }

    public NineDotsView(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.NineDotsView);

        final int N = array.getIndexCount();
        for (int i=0; i<N; i++) {
            int attr = array.getIndex(i);
            switch (attr) {
                case R.styleable.NineDotsView_hasChecked:
                    this.hasChecked = array.getBoolean(attr, false);
                    break;
                case R.styleable.NineDotsView_number:
                    this.myNumber = array.getString(attr);
                    break;
            }
        }

        this.setBackgroundResource(R.drawable.events_button);
        array.recycle();
    }
}
